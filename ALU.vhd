library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALU is
	generic(
		 DATA_WIDTH : integer := 32
		 ); 
    Port (
		  DATA_1 		: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
		  DATA_2			: in STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
		  ADD_SUB 		: in STD_LOGIC;
		  ABS_VAL 		: in STD_LOGIC;
		  AND_OR			: in STD_LOGIC;
		  NOR_NAND		: in STD_LOGIC;
		  NORNANDOR		: in STD_LOGIC;
		  
		  RES_OUT		: out STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
		  EQ				: out STD_LOGIC;
        LT				: out STD_LOGIC;
		  GT 				: out STD_LOGIC
		  
    );
end ALU;

architecture ALU of ALU is

signal adderOut : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
signal subOut   : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
signal andOut   : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
signal orOut    : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
signal nandOut  : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
signal norOut   : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);

signal addsubOut  : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
signal andorOut   : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
signal nornandOut : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
signal absOut     : STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);

begin

	adder32 : work.ADDER port map (
		IN1 => DATA_1,
		IN2 => DATA_2,
		Q => adderOut
	);  -- TODO Cout to CR?
	
	sub32 : work.SUB port map(
		IN1 => DATA_1,
		IN2 => DATA_2,
		Q => subOut
	); -- TODO Ficke ur mom?
	
	absval32 : work.ABSVAL port map(
		INP => DATA_1,
		Q => absOut
	); -- TODO Ficke ur dad?
	
	andOut  <= DATA_1 and  DATA_2;
	orOut   <= DATA_1 or   DATA_2;
	nandOut <= DATA_1 nand DATA_2;
	norOut  <= DATA_1 nor  DATA_2;
	
	addsubOut <= adderOut WHEN ADD_SUB = '1' ELSE
		subOut;
		
	andorOut <= andOut WHEN AND_OR = '1' ELSE
		orOut;
	
	nornandOut <= nandOut WHEN NOR_NAND = '1' ELSE
		norOut;
		
	RES_OUT <= addsubOut WHEN (ABS_VAL = '0' AND NORNANDOR = '0') ELSE
		andorOut WHEN (ABS_VAL = '0' AND NORNANDOR = '1') ELSE
		nornandOut WHEN (ABS_VAL = '1' AND NORNANDOR = '0') ELSE
		absOut;

end architecture ALU;