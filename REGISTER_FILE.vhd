library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity RegisterFile is
	generic(
		DATA_WIDTH : integer := 32;
		REGISTER_ADDR_WIDTH : integer := 4
		); 
    Port (
		CLK 			: in 	STD_LOGIC;
		WREN			: in 	STD_LOGIC; -- write enable - needs to be in '1' for DATA to be written into reg with the address of ADDR3
		UP_DOWN		: in 	STD_LOGIC; -- because of immediate value being 16b and regs being 32b long, we need a way to write upper/lower half
		FULL			: in 	STD_LOGIC; -- if we get a result from from ALU, it's 32b, so we don't want only lower/upper half
		ADR1			: in 	STD_LOGIC_VECTOR(REGISTER_ADDR_WIDTH - 1 downto 0); -- address of reg to go to REG_OUT_1 in the next clock cycle
		ADR2			: in 	STD_LOGIC_VECTOR(REGISTER_ADDR_WIDTH - 1 downto 0); -- address of reg to go to REG_OUT_2 in the next clock cycle
		ADR3			: in 	STD_LOGIC_VECTOR(REGISTER_ADDR_WIDTH - 1 downto 0); -- address of reg we want to write DATA to 
		DATA			: in 	STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
		REG_OUT_1	: out STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0); -- contents of reg addressed by ADDR1
		REG_OUT_2	: out STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0)  -- contents of reg addressed by ADDR1
    );
end RegisterFile;

architecture RegisterFile of RegisterFile is

type t_Registers is array (0 to 15) of std_logic_vector(DATA_WIDTH - 1 downto 0);
signal registers : t_Registers;


begin
	
	--registers(0)(4) <= '1';
	
	REGs : process (CLK, registers, ADR1, ADR2) 
	begin 
		if rising_edge(CLK) then 
			if WREN = '1' then 
			
				if FULL = '1' then 
					registers(to_integer(unsigned(ADR3))) <= DATA;
				elsif UP_DOWN = '1' then
					registers(to_integer(unsigned(ADR3)))(DATA_WIDTH - 1 downto DATA_WIDTH/2) <= DATA(DATA_WIDTH/2 - 1 downto 0);
				elsif UP_DOWN = '0' then 
					registers(to_integer(unsigned(ADR3)))(DATA_WIDTH/2 - 1 downto 0) <= DATA(DATA_WIDTH/2 - 1 downto 0);
				end if;
				
			end if;			
		end if;	
		REG_OUT_1 <= registers(to_integer(unsigned(ADR1)));
		REG_OUT_2 <= registers(to_integer(unsigned(ADR2)));
	end process;
	

end architecture RegisterFile;