library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SUB is
	generic(
		 INPUT_WIDTH : integer := 32;
		 OUTPUT_WIDTH : integer := 32
		 ); 
    Port (
        IN1, IN2  : in	STD_LOGIC_VECTOR(INPUT_WIDTH - 1 downto 0);
		  Q 			: out STD_LOGIC_VECTOR(OUTPUT_WIDTH - 1 downto 0)
    );
end SUB;

architecture AddInverse of SUB is

	signal complement : STD_LOGIC_VECTOR(OUTPUT_WIDTH - 1 downto 0);

begin
	
	Complementor : work.COMPLEMENTOR port map (
		INP => IN2, Q => complement
	);
	
	Adder : work.Adder port map (
		IN1 => IN1, IN2 => complement, Q => Q, Cout => open
	);
	
end architecture AddInverse;