library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FOKLORE is
    Port (
        CLOCK_50 : in  STD_LOGIC
		  
        
    );
end FOKLORE;

architecture CPU of FOKLORE is

signal instruction : STD_LOGIC_VECTOR(32 - 1 downto 0);
signal mov 			:  STD_LOGIC; -- wheter it's a move instruction or not (MOV has 16b immediate to move into register, so it's quite a different instruction)
signal reg_wren 		:  STD_LOGIC; -- register file write enable (we wanna write result of the instruction into register)
signal store 			:  STD_LOGIC; -- RAM write enable (we want to write register content into memory [addressed by another register])
signal reg_wr_up 	:  STD_LOGIC; -- UP_DOWN signal to Register File
signal full 		: STD_LOGIC;
signal jmp 			:  STD_LOGIC; -- JMP instruction (PC write enable pretty much)
signal load 			:  STD_LOGIC; -- We wanna read RAM and store the result into register
signal add_sub		:  STD_LOGIC; -- Wheter we want ALU to Add or Subtract the inputs
signal abs_val 		:  STD_LOGIC; -- Whether we want absolute value of one of the ALU inputs
signal and_or 		:  STD_LOGIC; -- Whether we want to bitwise AND or OR the ALU inputs
signal nor_nand 		:  STD_LOGIC; -- Whether we want to bitwise NAND or NOR the ALU inputs  
signal jeq				:  STD_LOGIC;
signal jgt_lt			:  STD_LOGIC;
signal nornandor		: 	STD_LOGIC;
signal reg_src_1 	: STD_LOGIC_VECTOR(4 - 1 downto 0); -- Address of the first register to go as ALU input
signal reg_src_2 	: STD_LOGIC_VECTOR(4 - 1 downto 0); -- Address of the second register to go as ALU input
signal reg_dest 		:  STD_LOGIC_VECTOR(4 - 1 downto 0); -- Address of the target register to store the ALU result
signal imm				:  STD_LOGIC_VECTOR(32 - 1 downto 0); -- Immediate value for the MOV

signal pc_out : STD_LOGIC_VECTOR (12 downto 0);
signal pc_wren : STD_LOGIC;
signal pc_addr : STD_LOGIC_VECTOR(31 downto 0);

signal reg_in : STD_LOGIC_VECTOR (31 downto 0);
signal reg_out_1 : STD_LOGIC_VECTOR (31 downto 0);
signal reg_out_2 : STD_LOGIC_VECTOR (31 downto 0); 

signal ram_data : STD_LOGIC_VECTOR (31 downto 0);
signal alu_out : STD_LOGIC_VECTOR (31 downto 0);
signal alu_eq : STD_LOGIC;
signal alu_lt : STD_LOGIC;
signal alu_gt : STD_LOGIC;

signal ram_alu_data : STD_LOGIC_VECTOR (31 downto 0);

signal alu_cmp_out : STD_LOGIC;

begin

	PC : work.ProgCntr PORT MAP (
		CLK 		=> CLOCK_50,
		JUMP_ADDR => pc_addr,
		WREN 		=> pc_wren,
		Q 			=> pc_out
	);
	
	ROMI_inst : work.ROMI PORT MAP (
		address => pc_out,
		clock	=> CLOCK_50,
		q	=> instruction	
	);
	
	RAMD_inst : work.RAMD PORT MAP (
		ADDRESS 	=> reg_out_1(13 - 1 downto 0),
		CLOCK		=> CLOCK_50,
		DATA		=> reg_out_2,
		WREN		=> store,
		Q			=> ram_data
	);
	
	DECODER_inst : work.DECODER PORT MAP (
		INSTRUCTION => instruction,
		MOV => mov,
		REG_WREN => reg_wren,
		STORE => store,
		REG_WR_UP => reg_wr_up,
		FULL => full,
		JMP => jmp,
		LOAD => load,
		ADD_SUB => add_sub,
		ABS_VAL => abs_val,
		AND_OR => and_or,
		NOR_NAND => nor_nand,
		JEQ => jeq,
		JGT_LT => jgt_lt,
		NORNANDOR => nornandor,
		REG_SRC_1 => reg_src_1,
		REG_SRC_2 => reg_src_2,
		REG_DEST => reg_dest,
		IMM => imm(16 - 1 downto 0)	  
	);
	
	REGISTERFILE_inst : work.RegisterFile PORT MAP (
		CLK 			=> CLOCK_50,
		WREN			=> reg_wren,
		UP_DOWN		=> reg_wr_up,
		FULL			=> full,
		ADR1			=> reg_src_1,
		ADR2			=> reg_src_2,
		ADR3			=> reg_dest,
		DATA			=> reg_in,
		REG_OUT_1	=> reg_out_1,
		REG_OUT_2	=> reg_out_2
	);
	
	ALU_inst : work.ALU PORT MAP (
		DATA_1 => reg_out_1,
		DATA_2 => reg_out_2,			
		ADD_SUB => 	add_sub,	
		ABS_VAL =>	abs_val,
		AND_OR  => 	and_or,
		NOR_NAND	=> nor_nand,
		NORNANDOR => nornandor,

		RES_OUT	=>	alu_out,
		EQ			=> alu_eq,
		LT			=> alu_lt,
		GT 		=> alu_gt
	);
	
	pc_addr <= reg_out_1;
	
	imm(32 - 1 downto 16) <= (others => '0');
	
	ram_alu_data <= ram_data when load = '1' else
						 alu_out;
						
	reg_in <= imm when mov = '1' else 
				 ram_alu_data;
	
	alu_cmp_out <= '0' when jeq = '1' AND jgt_lt = '1' else
						alu_lt when jeq = '0' AND jgt_lt = '0' else 
						alu_gt when jeq = '0' AND jgt_lt = '1' else 
						alu_eq;
					
	pc_wren <= jmp OR alu_cmp_out;

end architecture CPU;