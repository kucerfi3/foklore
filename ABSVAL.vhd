library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ABSVAL is
	generic(
		 INPUT_WIDTH : integer := 32;
		 OUTPUT_WIDTH : integer := 32
		 ); 
    Port (
        INP       : in	STD_LOGIC_VECTOR(INPUT_WIDTH - 1 downto 0);
		  Q 			: out STD_LOGIC_VECTOR(OUTPUT_WIDTH - 1 downto 0)
    );
end ABSVAL;

architecture AbsMaker of ABSVAL is

	signal compl_in : STD_LOGIC_VECTOR(OUTPUT_WIDTH - 1 downto 0);

begin
	Complementor : work.COMPLEMENTOR port map (
		INP => INP, Q => compl_in
	);
	
	Q <= INP WHEN INP(INP'HIGH) = '0' ELSE
		compl_in;
	
end architecture AbsMaker;