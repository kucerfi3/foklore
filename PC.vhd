library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ProgCntr is
	generic(
		 ADDR_WIDTH : integer := 32;
		 INST_ADDR_WIDTH : integer := 13
		 ); 
    Port (
        CLK 		: in  STD_LOGIC;
		  JUMP_ADDR : in	STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
		  WREN 		: in	STD_LOGIC;
		  Q 			: out STD_LOGIC_VECTOR(INST_ADDR_WIDTH - 1 downto 0)
    );
end ProgCntr;

architecture ProgCntr of ProgCntr is

	signal PC 	 : STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
	signal PCinc : STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);

begin
	
	Adder : work.ADDER port map (
		IN1 => PC, IN2 => (0 => '1', others => '0'), Q => PCinc, Cout => open
	);
	
	process(CLK, PC) 
	begin
		if rising_edge(CLK) then 
			if WREN = '1' then 
				PC <= JUMP_ADDR;		
			else 
				PC <= PCinc;
			end if;
		end if;
		Q <= PC(INST_ADDR_WIDTH - 1 downto 0);
	end process;

end architecture ProgCntr;