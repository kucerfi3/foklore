library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity LT is
	generic(
		 INPUT_WIDTH : integer := 32;
		 OUTPUT_WIDTH : integer := 32
		 ); 
    Port (
        IN1, IN2  : in	STD_LOGIC_VECTOR(INPUT_WIDTH - 1 downto 0);
		  Q 			: out STD_LOGIC
    );
end LT;

architecture LessThan of LT is

	signal s_and    : STD_LOGIC_VECTOR(INPUT_WIDTH - 1 downto 0);
	signal s_or     : STD_LOGIC_VECTOR(INPUT_WIDTH - 1 downto 0);
	signal carry_or : STD_LOGIC_VECTOR(INPUT_WIDTH - 1 downto 0);
	signal carry_sandals : 

begin
	
	s_and(0) <= not IN1(0) and IN2(0);
	
	Andors:
	for I in 1 to INPUT_WIDTH - 1 generate
      s_or(I - 1) <= not IN1(I - 1) or IN2(I - 1);
      --s_and(I) <= (and s_or(I - 1 downto 0)) and (not IN1(I) and IN2(I));
		Kondor:
		for K in 0 to I - 1 downto 0 generate
			s_and(I)
		end generate Kondor;
   end generate Andors;
    
	carry_or(0) <= s_and(0);
	
	Ors:
	for J in 1 to INPUT_WIDTH - 1 generate
		carry_or(J) <= carry_or(J - 1) or s_and(J);
	end generate Ors;
	
	Q <= carry_or(carry_or'HIGH);
	
end architecture LessThan;