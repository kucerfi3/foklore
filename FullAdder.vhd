library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FullAdder is
    Port (
        A, B, Cin 		: in	STD_LOGIC;
		  Q, Cout 	: out  STD_LOGIC
    );
end FullAdder;

architecture RCAdder of FullAdder is

	signal Qinter	: STD_LOGIC;
	signal Cinter	: STD_LOGIC_VECTOR(1 downto 0);
	
begin
	
	ha1 : work.HalfAdder port map (
		A => A, B => B, Q => Qinter, Cout => Cinter(0)
	);
	
	ha2 : work.HalfAdder port map (
		A => Qinter, B => Cin, Q => Q, Cout => Cinter(1)
	);
	
	Cout <= Cinter(0) or Cinter(1);
	
end architecture RCAdder;