library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ADDER is
	generic(
		 INPUT_WIDTH : integer := 32;
		 OUTPUT_WIDTH : integer := 32
		 ); 
    Port (
        IN1, IN2  : in	STD_LOGIC_VECTOR(INPUT_WIDTH - 1 downto 0);
		  Q 			: out STD_LOGIC_VECTOR(OUTPUT_WIDTH - 1 downto 0);
		  Cout 		: out STD_LOGIC
    );
end ADDER;

architecture RCAdder of ADDER is

	signal carries : STD_LOGIC_VECTOR(OUTPUT_WIDTH - 1 downto 0);

begin
	
	HA : work.HalfAdder port map (
		A => IN1(0), B => IN2(0), Q => Q(0), Cout => carries(0)
	);
	
	Adders: 
   for I in 1 to OUTPUT_WIDTH - 1 generate
      Adder : work.FullAdder port map
        (A => IN1(I) , B => IN2(I), Cin => carries(I - 1), Q => Q(I) , Cout => carries(I));
   end generate Adders;
	
	Cout <= carries(OUTPUT_WIDTH - 1);
	
end architecture RCAdder;