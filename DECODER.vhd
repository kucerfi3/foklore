library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Decoder is
	generic(
		 INSTR_WIDTH : integer := 32;
		 REGISTER_ADDR_WIDTH : integer := 4;
		 IMMEDIATE_VALUE_WIDTH : integer := 16
		 ); 
    Port (
		  INSTRUCTION 	: in 	STD_LOGIC_VECTOR(INSTR_WIDTH - 1 downto 0); -- instruction to decode
		  MOV 			: out STD_LOGIC; -- wheter it's a move instruction or not (MOV has 16b immediate to move into register, so it's quite a different instruction)
		  REG_WREN 		: out STD_LOGIC; -- register file write enable (we wanna write result of the instruction into register)
		  STORE 			: out STD_LOGIC; -- RAM write enable (we want to write register content into memory [addressed by another register])
		  REG_WR_UP 	: out STD_LOGIC; -- UP_DOWN signal to Register File
		  FULL			: out STD_LOGIC;
		  JMP 			: out STD_LOGIC; -- JMP instruction (PC write enable pretty much)
		  LOAD 			: out STD_LOGIC; -- We wanna read RAM and store the result into register
		  ADD_SUB		: out STD_LOGIC; -- Wheter we want ALU to Add or Subtract the inputs
		  ABS_VAL 		: out STD_LOGIC; -- Whether we want absolute value of one of the ALU inputs
		  AND_OR 		: out STD_LOGIC; -- Whether we want to bitwise AND or OR the ALU inputs
		  NOR_NAND 		: out STD_LOGIC; -- Whether we want to bitwise NAND or NOR the ALU inputs  
		  JEQ				: out STD_LOGIC;
		  JGT_LT			: out STD_LOGIC;
		  NORNANDOR		: out STD_LOGIC;
		  REG_SRC_1 	: out STD_LOGIC_VECTOR(REGISTER_ADDR_WIDTH - 1 downto 0); -- Address of the first register to go as ALU input
		  REG_SRC_2 	: out STD_LOGIC_VECTOR(REGISTER_ADDR_WIDTH - 1 downto 0); -- Address of the second register to go as ALU input
		  REG_DEST 		: out STD_LOGIC_VECTOR(REGISTER_ADDR_WIDTH - 1 downto 0); -- Address of the target register to store the ALU result
		  IMM				: out STD_LOGIC_VECTOR(IMMEDIATE_VALUE_WIDTH - 1 downto 0) -- Immediate value for the MOV instruction to store some value into register (upper / lower half only, see REG_WR_UP bit)
        
    );
end Decoder;

architecture Decoder of Decoder is
begin


end architecture Decoder;