library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity COMPLEMENTOR is
	generic(
		 INPUT_WIDTH  : integer := 32;
		 OUTPUT_WIDTH : integer := 32
		 ); 
    Port (
        INP       : in	STD_LOGIC_VECTOR(INPUT_WIDTH - 1 downto 0);
		  Q 			: out STD_LOGIC_VECTOR(OUTPUT_WIDTH - 1 downto 0)
    );
end COMPLEMENTOR;

architecture TwoSComplementor of COMPLEMENTOR is

	signal inverse_in : STD_LOGIC_VECTOR(INPUT_WIDTH - 1 downto 0);

begin
	
	inverse_in <= not INP;
	
	Adder : work.ADDER port map (
		IN1 => inverse_in, IN2 => (0 => '1', others => '0'), Q => Q, Cout => open
	);

	
end architecture TwoSComplementor;