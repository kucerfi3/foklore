library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity HalfAdder is
    Port (
        A, B  		: in	STD_LOGIC;
		  Q, Cout 	: out  STD_LOGIC
    );
end HalfAdder;

architecture RCAdder of HalfAdder is
begin
		
	Q 		<= A xor B; -- ( (A or B) and (not A and not B)); 	vzpominame
	Cout  <= A and B;
	
end architecture RCAdder;